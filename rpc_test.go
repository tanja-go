package tanja

import (
	"reflect"
	"testing"
)

type sT struct {
	tst         *testing.T
	ses         *Session
	thiscalled  int
	replycalled int
	closecalled int
	varcalled   int
}

var testmap map[string]Element = map[string]Element{
	"a":     El("val"),
	"stttr": El(nil),
}

var testslice []Element = []Element{El(0), El("")}

func (t *sT) ExportThis(a float32, b uint8, c string, d bool, e map[string]Element, f []Element) {
	if a != 10.5 {
		t.tst.Errorf("a == %v != 10.5", a)
	}
	if b != 42 {
		t.tst.Errorf("b == %v != 42", a)
	}
	if c != "str" {
		t.tst.Errorf("c == %v != str", c)
	}
	if !d {
		t.tst.Errorf("d == %v != true", d)
	}
	if !reflect.DeepEqual(e, testmap) {
		t.tst.Errorf("e == %v != %v", e, testmap)
	}
	if !reflect.DeepEqual(f, testslice) {
		t.tst.Errorf("f == %v != %v", f, testslice)
	}
	t.thiscalled++
}

func (t *sT) ExportVariadic(a int, e ...Element) {
	t.varcalled++
	err := true
	switch t.varcalled {
	case 1:
		err = a != 10 || len(e) != 1 || e[0].String() != "extra"
	case 2:
		err = a != 2 || len(e) != 0
	case 3:
		err = a != 3 || !reflect.DeepEqual(e, []Element{El(testmap)})
	}
	if err {
		t.tst.Errorf("Unexpected variadic argument(%d): %d, %#v", t.varcalled, a, e)
	}
}

func (t *sT) ExportReply(m *Message, s string) {
	if s != "10" {
		t.tst.Errorf("ExportReply string argument is '%s', expected '%s'", s, "argument")
	}
	if len(m.Tup) != 5 || m.Tup[4].String() != "extra" {
		t.tst.Errorf("ExportReply invalid tuple length or missing extra element")
	}
	m.Reply(len(s))
	m.Close()
	t.replycalled++
}

func (t *sT) ExportClose(s string) {
	t.closecalled++
	if s == "really!" {
		t.ses.Close()
	}
}

func (t *sT) DontExport() {
	t.tst.Error("DontExport() called.")
}

func TestRPC(tst *testing.T) {
	node := NewNode()
	ses := node.Session()

	obj := &sT{tst: tst, ses: ses}
	lst := ses.RegPrefix(obj, Tup("prefix", 1), "Export", true)
	if len(lst) != 4 {
		tst.Fatalf("Number of exported methods is %d, expected %d", len(lst), 4)
	}

	go func() {
		// These shouldn't match anything
		ses.Send(false)
		ses.Send(false, "")
		ses.Send(false, "Prefix", 1, "close", 1)
		ses.Send(false, "prefix", 2, "close", 1)
		ses.Send(false, "prefix", 1, "Close", 1)

		// Try replying (tuple should be received by This, Close, Reply and Variadic)
		r := ses.Send(true, "prefix", 1, nil, 10, "extra")
		if t := <-r.Chan(); !reflect.DeepEqual(t, Tup(2)) || <-r.Chan() != nil {
			tst.Error("Received invalid reply")
		}
		r.Close()

		// Should match and close immediately.
		r = ses.Send(true, "prefix", 1, "close", 10.2)
		if <-r.Chan() != nil {
			tst.Error("Close did not immediately close the return path")
		}
		r.Close()

		// Variadic test
		ses.Send(false, "prefix", 1, "variadic", 2)
		ses.Send(false, "prefix", 1, "variadic", 3, testmap)

		// Testing various types
		ses.Send(false, "prefix", 1, "this", 10.5, "42", "str", "some_true_value", testmap, testslice)

		// Shouldn't match
		ses.Send(false, "prefix", 1, "this", []Element{}, "42", "str", "some_true_value", testmap, testslice)
		ses.Send(false, "prefix", 1, "this", 10.5, nil, "str", "some_true_value", testmap, testslice)
		ses.Send(false, "prefix", 1, "this", 10.5, "42", "str", "some_true_value", testmap, 0)
		ses.Send(false, "prefix", 1, "this", 10.5, "42", "str", "some_true_value", 1, testslice)

		// Actual close
		ses.Send(false, "prefix", 1, "close", "really!")
		// Shouldn't be received
		ses.Send(false, "prefix", 1, nil, "str")
	}()

	ses.Run()
	if obj.thiscalled != 1 {
		tst.Errorf("This is called %d times, expected %d", obj.thiscalled, 1)
	}
	if obj.closecalled != 3 {
		tst.Errorf("Close is called %d times, expected %d", obj.closecalled, 3)
	}
	if obj.replycalled != 1 {
		tst.Errorf("Reply is called %d times, expected %d", obj.replycalled, 1)
	}
	if obj.varcalled != 3 {
		tst.Errorf("Variadic is called %d times, expected %d", obj.varcalled, 3)
	}
}
