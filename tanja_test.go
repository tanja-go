package tanja

import "testing"
import "reflect"

func TestSess(tst *testing.T) {
	node := NewNode()
	if node == nil {
		tst.Errorf("NewNode() returned nil")
	}
	done := make(chan int)

	go func() {
		next := 0
		havequit := false
		ses := node.Session()
		if ses == nil {
			tst.Errorf("Session()(1) returned nil")
		}
		reg := ses.Register(false, "test")
		reg.Callback(func(m *Message) {
			// Calling some arbitrary reply methods shouldn't matter at all if
			// the pattern is registered with !willReply.
			if next == 1 {
				m.Close()
			} else if next == 2 {
				m.Reply("blah")
			}
			// Tuples must be received in order
			if !reflect.DeepEqual(m.Tup, Tup("test", next)) {
				tst.Errorf("Unexpected Tuple: %v", m.Tup)
			}
			// Returning false should prevent this callback from being called again
			next++
			if next > 6 {
				tst.Errorf("Tuple after unregistering: %v", m.Tup)
			} else if next == 6 {
				reg.Close()
			}
		})
		ses.Register(true, "quit").Callback(func(m *Message) {
			m.Reply("reply", 0)
			m.Reply("reply", 1)
			m.Close()
			havequit = true
			ses.Close()
		})
		ses.Register(false, "last").Callback(func(m *Message) {
			tst.Errorf("Tuple after closing: %v", m.Tup)
		})
		done <- 1
		ses.Run()
		// next should be 6
		if next < 6 {
			tst.Errorf("Not all test Tuples have been received (next=%d)", next)
		}
		// Run shouldn't return before Close()
		if !havequit {
			tst.Errorf("Run() stopped before Close()")
		}
		done <- 1
	}()

	// Make sure the stuff is registered before spawning the sending session.
	<-done

	go func() {
		ses := node.Session()
		if ses == nil {
			tst.Errorf("Session()(2) returned nil")
		}
		for i := 0; i < 10; i++ {
			ses.Send(false, "test", i)
		}
		ret := ses.Send(true, "quit")
		ses.Send(false, "last")
		replies := 0
		for t := range ret.Chan() {
			if !reflect.DeepEqual(t, Tup("reply", replies)) {
				tst.Errorf("Unexpected reply: %v", t)
			}
			replies++
		}
		ses.Close()
		if replies != 2 {
			tst.Errorf("Replies != 2 (= %d)", replies)
		}
		done <- 1
	}()

	<-done
	<-done
}
