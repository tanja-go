package tanja

import (
	"encoding/json"
	. "strconv"
)

type Element struct {
	// This is either nil, int64, string, float64, []Element or map[string]Element
	E interface{}
}

type Tuple []Element

func (e Element) IsInt() (int64, bool) {
	switch v := e.E.(type) {
	case int64:
		return v, true
	case string:
		n, r := ParseInt(v, 10, 64)
		if r != nil {
			return 0, false
		} else {
			return n, true
		}
	case float64:
		return int64(v), true
	}
	return 0, false
}

// Just returns 0 on error
func (e Element) Int() int64 {
	i, _ := e.IsInt()
	return i
}

func (e Element) IsString() (string, bool) {
	switch v := e.E.(type) {
	case int64:
		return FormatInt(v, 10), true
	case string:
		return v, true
	case float64:
		return FormatFloat(v, 'g', -1, 64), true
	}
	return "", false
}

// Just returns empty string on error
func (e Element) String() string {
	s, _ := e.IsString()
	return s
}

func (e Element) IsFloat() (float64, bool) {
	switch v := e.E.(type) {
	case int64:
		return float64(v), true
	case string:
		n, r := ParseFloat(v, 64)
		if r != nil {
			return 0, false
		} else {
			return n, true
		}
	case float64:
		return v, true
	}
	return 0, false
}

// Just returns 0 on error
func (e Element) Float() float64 {
	f, _ := e.IsFloat()
	return f
}

func (e Element) Bool() bool {
	switch v := e.E.(type) {
	case nil:
		return false
	case int64:
		return v != 0
	case float64:
		return v != 0
	case string:
		return !(v == "" || v == "0")
	}
	return true
}

// Whether this is a wildcard or not
func (e Element) WC() bool {
	return e.E == nil
}

// Returns an empty slice if this is no array
func (e Element) Slice() []Element {
	if v, ok := e.E.([]Element); ok {
		return v
	}
	return []Element{}
}

// Returns a nil map if this is no map.
func (e Element) Map() map[string]Element {
	if v, ok := e.E.(map[string]Element); ok {
		return v
	}
	return nil
}

func (a Element) Match(b Element) bool {
	// Wildcards always match
	if a.WC() || b.WC() {
		return true
	}
	// Fuzzy int matching
	if ia, ca := a.IsInt(); ca {
		if ib, cb := b.IsInt(); cb && ia == ib {
			return true
		}
	}
	// Exact matching (works fine for strings)
	return a.E == b.E
}

func (e Element) MarshalJSON() (r []byte, err error) {
	r, err = json.Marshal(e.E)
	return
}

func (e *Element) UnmarshalJSON(b []byte) (err error) {
	switch b[0] {
	case 'n': // null
		e.E = nil
	case 't': // true
		e.E = int64(1)
	case 'f': // false
		e.E = int64(0)
	case '"': // string
		var s string
		err = json.Unmarshal(b, &s)
		e.E = s
	case '{': // map
		var m map[string]Element
		err = json.Unmarshal(b, &m)
		e.E = m
	case '[': // array
		var a []Element
		err = json.Unmarshal(b, &a)
		e.E = a
	default: // then this must be a number
		// The json package stores all numbers into a float64. We'd like to be
		// able to use the full range of an int64. Note that numbers formatted
		// with an exponent or fractional part are still interpreted as a
		// float, regardless of whether it can be represented as a int64 or
		// not.
		if n, r := ParseInt(string(b), 10, 64); r == nil {
			e.E = n
		} else {
			var f float64
			err = json.Unmarshal(b, &f)
			e.E = f
		}
	}
	return
}

// Mostly the same as Element{e}, but also converts any int/bool or float type to an
// int64 or float64, respectively.
// Throws a runtime panic if an unsupported type is given.
// TODO: Also convert composite types into arrays/maps?
func El(e interface{}) Element {
	switch v := e.(type) {
	// Core elements
	case nil, string, int64, float64, []Element, map[string]Element:
		return Element{v}
	// Already an element
	case Element:
		return v
	// Conversions
	case bool:
		if v {
			return Element{int64(1)}
		} else {
			return Element{int64(0)}
		}
	case int:
		return Element{int64(v)}
	case int8:
		return Element{int64(v)}
	case int16:
		return Element{int64(v)}
	case int32:
		return Element{int64(v)}
	case uint:
		return Element{int64(v)}
	case uint8:
		return Element{int64(v)}
	case uint16:
		return Element{int64(v)}
	case uint32:
		return Element{int64(v)}
	case uint64:
		if v > 1<<63-1 {
			panic("Value out of range")
		}
		return Element{int64(v)}
	case float32:
		return Element{float64(v)}
	}
	panic("Invalid type")
}

func (t Tuple) Match(p Tuple) bool {
	if len(t) < len(p) {
		return false
	}
	for i := range p {
		if !t[i].Match(p[i]) {
			return false
		}
	}
	return true
}

// Same as El(map[string]Element{a: El(b), c: El(d), ...})
// panics on incorrect arguments
func Map(t ...interface{}) map[string]Element {
	r := make(map[string]Element, len(t)/2)
	for i := 0; i < len(t); i += 2 {
		r[t[i].(string)] = El(t[i+1])
	}
	return r
}

// Same as []Element{El(a), El(b), ..})
func Array(t ...interface{}) []Element {
	r := make([]Element, len(t))
	for i, v := range t {
		r[i] = El(v)
	}
	return r
}

// Same as Tuple(Array(a, b, ...))
func Tup(t ...interface{}) Tuple {
	return Tuple(Array(t...))
}
