package tanja

import (
	. "sync"
)

type Node struct {
	lock   Mutex
	lastId int32
	regs   map[int32]*Registration
	links  map[*Link]bool
}

func NewNode() *Node {
	return &Node{regs: make(map[int32]*Registration), links: make(map[*Link]bool), lastId: 1}
}

func (n *Node) unregOne(r *Registration) {
	for l := range n.links {
		l.nodeUnreg(r.id)
	}

	r.active = false
	r.unreg()
	delete(n.regs, r.id)
}

func (n *Node) unregMatch(f func(r *Registration) bool) {
	for _, r := range n.regs {
		if f(r) {
			n.unregOne(r)
		}
	}
}

func (n *Node) reg(r *Registration) {
	old := n.lastId
	for old == n.lastId || n.regs[n.lastId] != nil {
		n.lastId++
		if n.lastId < 0 {
			n.lastId = 1
		}
	}
	r.id = n.lastId
	n.regs[n.lastId] = r
	r.active = true

	for l := range n.links {
		l.nodeReg(r)
	}
}

func (n *Node) send(t Tuple, path *ReturnPath, ign interface{}) {
	// TODO: Some way of preventing that a message is routed back to the link
	// it came from.
	for _, r := range n.regs {
		if ign != r.recv && t.Match(r.pat) {
			var p *ReturnPath
			if r.willReply {
				p = path
				p.ref()
			}
			// Pass a reference to the Tuple to allow duplicate detection.
			r.send(&t, p)
		}
	}

	// Make sure to send path closed notification if there's nobody to reply.
	path.ref()
	path.unref()
}
